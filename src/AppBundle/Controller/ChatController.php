<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ChatController extends Controller
{
    /**
     * @Route("/Chat", name="Chatpage")
     */
    public function indexAction(Request $request)
    {
        //get All user
        $usr= $this->get('security.context')->getToken()->getUser()->getId();
        $AllUserQuery=$this->getDoctrine()->getRepository("AppBundle:User")->createQueryBuilder("e")
            ->select(" e.id,e.username")
            ->where("e.id != :id")
            ->setParameter("id",$usr)
        ;
        $AllUsers=$AllUserQuery->getQuery()->getResult();
        return $this->render('chat/index.html.twig',array('Alluser'=>$AllUsers));
    }
}